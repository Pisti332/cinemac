﻿using CinemaC.Models;

namespace CinemaC.Services
{
    public interface IMovieService
    {
        Task AddMovieAsync(Movie movie);
        Task<Movie> GetMovieByIdAsync(int  id);
        Task UpdateMovieById(int id, Movie movie);
        Task DeleteMovieByIdAsync(int  id);
    }
}
