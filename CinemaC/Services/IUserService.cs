﻿using CinemaC.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CinemaC.Services
{
    public interface IUserService
    {
        Task<List<User>> GetAllUsersAsync();
        Task<User> GetUserByIdAsync(int userId);
        Task AddUserAsync(User user);
    }
}
