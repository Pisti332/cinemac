﻿using CinemaC.Data;
using CinemaC.Exeptions;
using CinemaC.Models;

namespace CinemaC.Services
{
    public class ScreeningService : IScreeningService
    {
        private readonly CinemaCContext CinemaCContext;
        private readonly IRoomService RoomService;
        private readonly IMovieService MovieService;
        public ScreeningService(CinemaCContext CinemaCContext, IRoomService roomService, IMovieService movieService)
        {
            this.CinemaCContext = CinemaCContext;
            RoomService = roomService;
            MovieService = movieService;
        }

        public async Task AddScreeningAsync(Screening screening, int roomId, int movieId)
        {
            Room Room = await RoomService.GetRoomById(roomId);
            screening.Room = Room;

            Movie? Movie = await MovieService.GetMovieByIdAsync(movieId);
            screening.Movie = Movie;
            await CinemaCContext.Screening.AddAsync(screening);
            await CinemaCContext.SaveChangesAsync();
        }

        public async Task<Screening> GetScreeningById(int screeningId)
        {
            Screening? Screening = await CinemaCContext.Screening.FindAsync(screeningId);
            if (Screening == null)
            {
                throw new NotFoundException("Screening is not found!");
            }
            return Screening;
        }
        public async Task DeleteScreeningByIdAsync(int id)
        {
            Screening? Screening = await GetScreeningById(id);
            if (Screening == null)
            {
                throw new NotFoundException("Screening is not found!");
            }
            CinemaCContext.Screening.Remove(Screening);
            await CinemaCContext.SaveChangesAsync();
        }
    }
}
