﻿using CinemaC.Data;
using CinemaC.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CinemaC.Services
{
    public class UserService : IUserService
    {
        private readonly CinemaCContext CinemaCContext;
        public UserService(CinemaCContext CinemaCContext) {
            this.CinemaCContext = CinemaCContext;
        }

        public async Task AddUserAsync(User User)
        {
            User.RegistryDate = DateTime.UtcNow;
            await CinemaCContext.Users.AddAsync(User);
            await CinemaCContext.SaveChangesAsync();
        }

        public async Task<List<User>> GetAllUsersAsync()
        {
            return await CinemaCContext.Users.ToListAsync();
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await CinemaCContext.Users.FindAsync(userId);
        }
    }
}
