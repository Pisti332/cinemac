﻿using CinemaC.Models;

namespace CinemaC.Services
{
    public interface IScreeningService
    {
        Task AddScreeningAsync(Screening screening, int roomId, int movieId);
        Task<Screening> GetScreeningById(int screeningId);
        Task DeleteScreeningByIdAsync(int id);
    }
}
