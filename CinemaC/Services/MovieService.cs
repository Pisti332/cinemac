﻿using CinemaC.Data;
using CinemaC.Exeptions;
using CinemaC.Models;

namespace CinemaC.Services
{
    public class MovieService : IMovieService
    {
        private readonly CinemaCContext CinemaCContext;
        public MovieService(CinemaCContext CinemaCContext)
        {
            this.CinemaCContext = CinemaCContext;
        }
        public async Task AddMovieAsync(Movie movie)
        {
            await CinemaCContext.Movie.AddAsync(movie);
            await CinemaCContext.SaveChangesAsync();
        }
        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            Movie? Movie = await CinemaCContext.Movie.FindAsync(id);
            if (Movie == null)
            {
                throw new NotFoundException("Movie is not found!");
            }
            return Movie;
        }
        public async Task UpdateMovieById(int id, Movie movie)
        {
            Movie? Movie = await GetMovieByIdAsync(id);

            Movie.Name = movie.Name;
            Movie.Description = movie.Description;
            Movie.LengthInSeconds = movie.LengthInSeconds;

            await CinemaCContext.SaveChangesAsync();
        }
        public async Task DeleteMovieByIdAsync(int id)
        {
            Movie? Movie = await GetMovieByIdAsync(id);
            if (Movie == null)
            {
                throw new NotFoundException("Movie not found!");
            }
            CinemaCContext.Movie.Remove(Movie);
            await CinemaCContext.SaveChangesAsync();
        }


    }
}
