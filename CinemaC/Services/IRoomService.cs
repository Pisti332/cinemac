﻿using CinemaC.Models;

namespace CinemaC.Services
{
    public interface IRoomService
    {
        Task AddRoomAsync(Room Room);
        Task RemoveRoomAsync(int RoomId);
        Task ChangeRoomAvailability(int RoomId, bool isAvailable);
        Task<Room> GetRoomById(int RoomId);
    }
}
