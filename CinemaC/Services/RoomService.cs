﻿using CinemaC.Data;
using CinemaC.Exeptions;
using CinemaC.Models;

namespace CinemaC.Services
{
    public class RoomService : IRoomService
    {
        private readonly CinemaCContext CinemaCContext;
        public RoomService(CinemaCContext CinemaCContext)
        {
            this.CinemaCContext = CinemaCContext;
        }
        public async Task AddRoomAsync(Room Room)
        {
            Room.IsAvailable = false;
            await CinemaCContext.Room.AddAsync(Room);
            await CinemaCContext.SaveChangesAsync();
        }

        public async Task ChangeRoomAvailability(int RoomId, bool isAvailable)
        {
            Room? Room = await GetRoomById(RoomId);
            if (Room == null)
            {
                throw new NotFoundException("Room not found!");
            }
            Room.IsAvailable = isAvailable;
            await CinemaCContext.SaveChangesAsync();
        }

        public async Task<Room> GetRoomById(int roomId)
        {
            Room? Room = await CinemaCContext.Room.FindAsync(roomId);
            if (Room == null)
            {
                throw new NotFoundException("Room is not found!");
            }
            return Room;
        }

        public async Task RemoveRoomAsync(int RoomId)
        {
            Room? Room = await CinemaCContext.Room.FindAsync(RoomId);
            if (Room == null)
            {
                throw new NotFoundException("Room is not found!");
            }
            CinemaCContext.Room.Remove(Room);
            await CinemaCContext.SaveChangesAsync();
        }
    }
}
