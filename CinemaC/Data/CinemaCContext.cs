﻿using Microsoft.EntityFrameworkCore;
using CinemaC.Models;

namespace CinemaC.Data
{
    public class CinemaCContext : DbContext
    {
        public CinemaCContext(DbContextOptions<CinemaCContext> options) : base(options)
        {
            
        }
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Movie> Movie { get; set; } = null!;
        public DbSet<Room> Room { get; set; } = null!;
        public DbSet<Screening> Screening { get; set; } = null!;

    }
}
