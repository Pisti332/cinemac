﻿using CinemaC.Models;
using CinemaC.Services;
using Microsoft.AspNetCore.Mvc;

namespace CinemaC.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScreeningController : ControllerBase
    {
        private readonly IScreeningService ScreeningService;
        public ScreeningController(IScreeningService screeningService)
        {
            this.ScreeningService = screeningService;
        }
        [HttpPost(Name = "addScreening")]
        public async Task<IActionResult> AddScreening([FromBody] Screening screening, [FromQuery] int roomID, [FromQuery] int movieId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                await ScreeningService.AddScreeningAsync(screening, roomID, movieId);
                return Created();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetScreeningById(int Id)
        {
            try
            {
                Screening Screening = await ScreeningService.GetScreeningById(Id);
                return Ok(Screening);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{screeningId}")]
        public async Task<IActionResult> DeleteScreeningById(int screeningId)
        {
            try
            {
                await ScreeningService.DeleteScreeningByIdAsync(screeningId);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
