﻿using CinemaC.Models;
using CinemaC.Services;
using Microsoft.AspNetCore.Mvc;

namespace CinemaC.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoomController : ControllerBase
    {
        private readonly IRoomService RoomService;
        public RoomController(IRoomService RoomService)
        {
            this.RoomService = RoomService;
        }

        [HttpPost(Name = "createRoom")]
        public async Task<IActionResult> AddRoom([FromBody] Room Room)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                await RoomService.AddRoomAsync(Room);
                return CreatedAtAction(nameof(GetRoomById), new { id = Room.ID }, Room);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetRoomById(int Id)
        {
            try
            {
                Room Room = await RoomService.GetRoomById(Id);
                return Ok(Room);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPatch(Name = "changeRoomAvailability")]
        public async Task<IActionResult> ChangeRoomAvailability(int RoomId, bool isAvailable)
        {
            try
            {
                await RoomService.ChangeRoomAvailability(RoomId, isAvailable);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpDelete("{RoomId}")]
        public async Task<IActionResult> DeleteRoomById(int RoomId)
        {
            try
            {
                await RoomService.RemoveRoomAsync(RoomId);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
