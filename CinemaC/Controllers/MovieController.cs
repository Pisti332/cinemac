﻿using CinemaC.Models;
using CinemaC.Services;
using Microsoft.AspNetCore.Mvc;

namespace CinemaC.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService MovieService;
        public MovieController(IMovieService movieService)
        {
            this.MovieService = movieService;
        }
        [HttpPost(Name = "addMovie")]
        public async Task<IActionResult> AddMovie([FromBody] Movie movie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                await MovieService.AddMovieAsync(movie);
                return CreatedAtAction(nameof (GetMovieById), new {id = movie.Id}, movie);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMovieById(int id)
        {
            try
            {
                Movie Movie = await MovieService.GetMovieByIdAsync(id);
                return Ok(Movie);
            }
            catch
            {
                return BadRequest();

            }

        }
        [HttpPatch("{id}", Name = "updateMovieById")]
        public async Task<IActionResult> UpdateMovieByIdAsync(int id, [FromBody] Movie movie)
        {
            try
            {
                await MovieService.UpdateMovieById(id, movie);
                return Ok();
            }
            catch (Exception ex)
            { 
                return BadRequest(ex);
            }
        }
        [HttpDelete("{id}", Name = "deleteMovieById")]
        public async Task<IActionResult> DeleteMovieById(int id)
        {
            try
            {
                await MovieService.DeleteMovieByIdAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
