﻿namespace CinemaC.Models
{
    public class Screening
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Room? Room { get; set; }
        public Movie? Movie { get; set; }
        public ICollection<User>? User { get; set; }

    }
}
