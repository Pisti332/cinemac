﻿namespace CinemaC.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
        public DateTime RegistryDate { get; set; }
        public ICollection<Screening>? Screenings { get; set; } = null!;
    }
}
