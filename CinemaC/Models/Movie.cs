﻿using System.ComponentModel.DataAnnotations;

namespace CinemaC.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        [Required]
        public int LengthInSeconds { get; set; }
        public ICollection<Screening>? Screenings { get; set; }
    }
}
