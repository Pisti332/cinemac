﻿using System.ComponentModel.DataAnnotations;

namespace CinemaC.Models
{
    public class Room
    {
        public int ID { get; set; }
        public string Name { get; set; } = null!;
        [Required]
        public int Limit { get; set; }
        public bool IsAvailable { get; set; }
        public ICollection<Screening>? Screenings { get; set; } = null!;


    }
}
