(() => {
    document.addEventListener("DOMContentLoaded", main)
})();

async function main() {
    const streamKey = "sk_eu-central-1_8MDKOYDoCOCh_gtsHo4UmeJA3nKTNco1iTvfjGVdghS";
    const streamConfigs = [
        IVSBroadcastClient.BASIC_LANDSCAPE,
        IVSBroadcastClient.BASIC_FULL_HD_LANDSCAPE,
        IVSBroadcastClient.STANDARD_LANDSCAPE,
        IVSBroadcastClient.BASIC_PORTRAIT,
        IVSBroadcastClient.BASIC_FULL_HD_PORTRAIT,
        IVSBroadcastClient.STANDARD_PORTRAIT
    ];
    const ingestEndpoint = 'rtmps://3e76b206f006.global-contribute.live-video.net:443/app/';

    const client = IVSBroadcastClient.create({
        // Enter the desired stream configuration
        streamConfig: streamConfigs[0],
        // Enter the ingest endpoint from the AWS console or CreateChannel API
        ingestEndpoint: ingestEndpoint,
        // maxFramerate: 30,

        //kbps
        maxBitrate: 3500,
    });
    await handlePermissions();
    preview(client);
    const videoDevices = await listDevices();

    const streamConfig = {
        maxResolution: {
            width: 1920,
            height: 1080
        }
    }

    await getVideoMediaStreamFromDevice(videoDevices, streamConfig);
    await getAudioMediaStreamFromDevice();

    client.addVideoInputDevice(window.cameraStream, 'camera1', { index: 0 }); // only 'index' is required for the position parameter
    client.addAudioInputDevice(window.microphoneStream, 'mic1');

    window.goLive = () => {
        startBroadcast(client, streamKey);
    }
    window.stopLive = () => {
        stopBroadcasting(client);
    }
    
}

async function handlePermissions() {
    let permissions = {
        audio: false,
        video: false,
    };
    try {
        const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
        // for (const track of stream.getTracks()) {
        //     track.stop();
        // }
        permissions = { video: true, audio: true };
    } catch (err) {
        permissions = { video: false, audio: false };
        console.error(err.message);
    }
    // If we still don't have permissions after requesting them display the error message
    if (!permissions.video) {
        console.error('Failed to get video permissions.');
    } else if (!permissions.audio) {
        console.error('Failed to get audio permissions.');
    }
}

function preview(client) {
    // where #preview is an existing <canvas> DOM element on your page
    const previewEl = document.getElementById('preview');
    client.attachPreview(previewEl);
}

async function listDevices() {
    // now only to console
    const devices = await navigator.mediaDevices.enumerateDevices();
    window.videoDevices = devices.filter((d) => d.kind === 'videoinput');
    window.audioDevices = devices.filter((d) => d.kind === 'audioinput');
    const videoDevices = devices.filter((devcie) => {
        if (devcie.kind == "videoinput") {
            console.log(devcie);
            return true;
        }
        else return false;
    });
    return videoDevices;
}

async function getVideoMediaStreamFromDevice(videoDevices, streamConfig) {
    window.cameraStream = await navigator.mediaDevices.getUserMedia({
        video: {
            // deviceId: window.videoDevices[0].deviceId,
            deviceId: videoDevices[0].deviceId,
            width: {
                ideal: streamConfig.maxResolution.width,
            },
            height: {
                ideal: streamConfig.maxResolution.height,
            },
        }
    });
}

async function getAudioMediaStreamFromDevice() {
    window.microphoneStream = await navigator.mediaDevices.getUserMedia({
        audio: { deviceId: window.audioDevices[0].deviceId },
    });
}

function startBroadcast(client, streamKey) {
    client
   .startBroadcast(streamKey)
   .then((result) => {
       console.log('I am successfully broadcasting!');
   })
   .catch((error) => {
       console.error('Something drastically failed while broadcasting!', error);
   });
}

function stopBroadcasting(client) {
    client.stopBroadcast();
}