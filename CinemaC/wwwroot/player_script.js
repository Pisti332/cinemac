(() => {
    const STREAMER_NAME = "Pisti332"
    document.addEventListener("DOMContentLoaded", main)
})();

function main() {
    const stramUrl = "https://3e76b206f006.eu-central-1.playback.live-video.net/api/video/v1/eu-central-1.891377368468.channel.ZaofKgLafljH.m3u8";
    const ivsPlayer = IVSPlayer.create();
    ivsPlayer.attachHTMLVideoElement(document.getElementById("video-player"));
    ivsPlayer.load(stramUrl);
    ivsPlayer.play();
    ivsPlayer.addEventListener(IVSPlayer.PlayerEventType.ERROR, (err) => {
        console.error(err);
        setTimeout(() => location.reload(), 1000);
    })
    ivsPlayer.addEventListener(IVSPlayer.PlayerState.PLAYING, () => {
        alert(`${STREAMER_NAME} is live!`);
    })
    ivsPlayer.addEventListener(IVSPlayer.PlayerState.ENDED, () => {
        alert(`${STREAMER_NAME}'s stream has ended! Check back later.`);
    })
    ivsPlayer.addEventListener(IVSPlayer.PlayerEventType.STATE_CHANGED, (state) => {
        // if (state == "Buffering") {
        //     setTimeout(() => {
        //         if (state == "Buffering") {
        //             alert("The stream has either ended, or the connection is lost...")
        //         }
        //     }, 5000)
        // }
        console.log(`The current state is: ${state}`)
    })
}